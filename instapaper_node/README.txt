# $Id$

Instapaper Node

--- Overview ---
This module is a simple implementation of the Instapaper API module that allows logged in users to add their Instapaper Username and Password to their Drupal account and then allow the user to add any node on the site to their Instapaper account.  Allows for anon users to input their Instapaper credentials and have the same functionality.

--- Version History ---
1.0 - First Release!

--- Planned Upgrades ---
 ~ Theme the module
 ~ Allow administrators to specify variables for items such as URL, Title, Selection, and Redirect
 ~ Add in Permissions 